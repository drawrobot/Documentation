#Known Issues
- The communication between the Raspberry PI and the Server is still very unstable
- Drawing takes way too long and has to be improved alot in the future (a DIN A4 image currently takes around 2 hours to draw)
- Generating the commands for the PI is still very inefficent. Although the drawn image looks very good, some improvements to the performance could be made (currently we are drawing an image line by line. It would be more efficent to start drawing the next line, as soon as the current line has no more "black pixels").