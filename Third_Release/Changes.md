# Changes

- Merged ImageProcessing with TCPServer
- Implemented Google Cloud Messaging for Clients and Server
- RaspberryPi now connects to the server automatically after startup
- RaspberryPi TCP Client and motor control is now merged

