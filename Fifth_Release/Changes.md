# Changes

## December Release
- Merged ImageProcessing with TCPServer
- Implemented Google Cloud Messaging for Clients and Server
- RaspberryPi now connects to the server automatically after startup
- RaspberryPi TCP Client and motor control is now merged

## Semester Release
- Bug fixes in the mobile apps
- Extensive testing and bug finding done
- Small performance improvements while drawing
- Stability improvements (drawing hours on end)

## Final Release
- Added multithreading
- Bug fixes in Apps
- final documentation