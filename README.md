# Sir Draw A Lot Manual
**Daniel Windhager, Michael Moser, Rene Ferrari**

![](lisa.jpg)

## Documentation
[Product Backlog](https://docs.google.com/spreadsheets/d/1TAz_fo0kg0WLuFrlcXZxwRaFph7kUr4OAlzayZd0u3U/edit?usp=sharing)

## Image Processing 
To use the our ImageProcessing infrastructure you will have to open our current 
project, which is in the GitLab Repository, in Visual Studio. Once you did that you have to supply the starting point for the robot, as well as the lengths of the string attached and the total width of the robot. After all of those parameters have been set you can start by adding points to which the robot should move, thus drawing a picture. To get the output for the Raspberry Pi / Simulator just run the project. The output will be written to the tmp.txt file which is located in the /bin/Debug/ folder of the Visual Studio Project.

## Raspberry Pi
### Hardware Setup
![](hardware.jpg)

Our hardware setup is actually relatively simple once you have the right positions figured out. First of all, we don't have an external energy source. Every current needed comes from the raspberry pi. 

Secondly both motors are connected to a breadboard. The GPIO Pins on the Raspberry Pi for the motors are shown below. The order in which to use those in order to move the motors is described as well.
When you run a program and the lights are flashing red (one after another), the motors are connected correctly.

**First Motor _(18, 23, 24, 25)_**

1. 25 ON **sleep** 25 OFF
2. 25 ON 24 ON **sleep** 25 OFF 24 OFF
3. 24 ON **sleep** 24 OFF
4. 23 ON 24 ON **sleep** 23 OFF 24 OFF
5. 23 ON **sleep** 23 OFF
6. 18 ON 23 ON **sleep** 18 OFF 23 OFF
7. 18 ON **sleep** 18 OFF
8. 25 ON 18 ON **sleep** 25 OFF 18 OFF

**Second Motor _(4, 17, 27, 22)_**

Same as above with the Pins for Motor 2.

To find out how a stepper motor works you can take a look at the following link: https://en.wikipedia.org/?title=Stepper_motor

### Software Setup
To let the robot draw a picture or a line you first need a valid input file which you can get from our ImageProcessing C# Project. If you have this just place the file (with the name "tmp.txt") in the same folder as the python files **move.py** and **steps.py** which are used for steering the motors. After this just start the robot by typing in:

`sudo python move.py`

**Sudo rights are necessary for controlling the GPIO PINS!**

## Simulator
Our simulator as well as the RaspberryPi needs a valid input file. After you drop this onto the file you start the simulator by pressing `PLAY`. Alternatively you could shorten the time for each frame by entering a value less than 100 in the field beside the file chooser. 

The rest of the commands are described below:

`CLEAR` Clears the drawing canvas. Does not reset intern variables and states.

`PAUSE` Pauses the drawing process. Does not reset intern variables and states.

`RESET` Stops the drawing process. Resets intern variables and states.

## TCPServer
To use our TCPServer for storing and applying a threshhold to images the only thing you have to do is establish a TCP Connection to our server which is currently located in Germany. 
Alternatively you can start a local TCPServer by double-clicking the `.exe` (make sure that the firewall is not blocking the port).

`HOST:` sirdrawalot.tk

`PORT:` 1521

`USERNAME:` Windi or Rene

`PASSWORD:` password

## Clients
Our Clients are structured and easy to understand. The only thing you will have to do is log in and you're good to go.