#Installation

##iOS
To start the iOS App open it with XCode and run it.
##Android
The Android App has been deployed and is available as an .apk file. This file is called "SirDrawALot-Release.apk" and can be found in the SirDrawALot folder. The .apk has to be uploaded to your phone and started.
##Server
For the server you just have to start the "TcpServer.exe" which can be found at the following path: "TcpServer/obj/Debug"
##Raspberry PI
Usually the PI connects to the Server automatically, however in some rare cases you might need to do it yourself:
<br/>
###Starting manually:<br/>
- ``sudo python client.py``
###Autostart
- write ``sudo python client.py`` into ``/etc/rc.local``