# Feature List - SirDrawALot

## December Release
The User is able to:

- Login using our designated mobile apps
- Take a picture with our apps
- Upload an image from your library/local storage
- Upload an image to the server to be drawn
- Upload text to the server to be drawn
- View uploaded pictures from server
- Choose the threshold value for an image to be drawn
- Delete an uploaded image from the server
- Order the server to draw an image
- Receive notifications when the drawing started or stopped
- Enjoy the experience with SirDrawALot

## Semester Release
- Software stability
- Stability to draw hours on end
- Draw larger images without sacrificing precision

*SirDrawALot-Team*